import { Model } from './Model'
import { UserProps } from '../types/IUserProps'
import { Attributes} from './Attributes'
import { ApiSync } from './apiSync'
import { Eventing } from './Eventing'
import { Collection } from './Collection'

export class User extends Model<UserProps> {
    static build(attrs: UserProps): User {
        return new User(
            new Attributes<UserProps>(attrs),
            new Eventing(),
            new ApiSync<UserProps>()
        )
    }

    isAdminUser(): boolean {
        return this.get('id') === 1
    }

    setRandomAge(): void {
        const age = Math.round(Math.random() * 80)

        this.set({ age })
    }

    static buildUserCollection(): Collection<User, UserProps> {
        return new Collection<User, UserProps>(
        (json: UserProps) => User.build(json)
        )
    }
}
