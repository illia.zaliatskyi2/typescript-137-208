import axios , { AxiosResponse } from 'axios'
import { Eventing } from './Eventing'
import { serverUrl } from '../variables'

export class Collection<T, K> {
    models: T[] = []
    events: Eventing = new Eventing()

    constructor(
        public deserialize: (json: K) => T
    ) {}

    get on() {
        return this.events.on
    }

    get trigger() {
        return this.events.trigger
    }

    fetch(): void {
        axios.get(`${serverUrl}users`)
            .then((response: AxiosResponse) => {
                response.data.forEach((userParams: K) => {
                    this.models.push(this.deserialize(userParams))
                })
            })
        this.trigger('change')
    }
}
