import axios, { AxiosPromise } from 'axios'
import { serverUrl } from '../variables'

interface HasId {
    id?: number
}

export class ApiSync<T extends HasId> {
    save(data: T): AxiosPromise {
        const { id } = data
        console.log('here is post with id', id)
        if(id) {
            return axios.put(`${serverUrl}users/${id}`, data)
        } else {
            return axios.post(`${serverUrl}users/`, data)
        }
    }

    fetch(id: number): AxiosPromise {
        return axios.get(`${serverUrl}users/${id}`)
    }
}
