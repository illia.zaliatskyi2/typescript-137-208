import { AxiosResponse } from 'axios'
import { Attributes } from '../types/IAttributes'
import { Events } from '../types/IEvents'
import { Sync } from '../types/ISync'

interface HasId {
    id?: number
}

export class Model<T extends HasId> {
    constructor(
        private attributes: Attributes<T>,
        private events: Events,
        private sync: Sync<T>
    ) {}

    on = this.events.on
    trigger = this.events.trigger
    get = this.attributes.get

    set(update: T ): void {
        this.attributes.set(update)
        this.events.trigger('change')
    }

    fetch(): void {
        const id = this.attributes.get('id')

        if (typeof id !== 'number') {
            throw new Error('have no id or it has not correct format')
        }

        this.sync.fetch(id).then(({data}: AxiosResponse ): void => {
            this.set(data)
        })
    }

    save(): void {
        this.sync.save(this.attributes.getAll())
            .then((response: AxiosResponse) => {
                this.trigger('save')
            })
            .catch(() => {
                this.trigger('error')
            })
    }
}
