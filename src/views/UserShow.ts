import { View } from './View'
import { User } from '../models/User'
import { UserProps } from '../types/IUserProps'

export class UserShow extends View<User, UserProps> {
    template(): string {
        return `
            <div>
                <h1>User Detail:</h1>
                <div>User name: ${this.model.get('name')}</div>
                <div>User age: ${this.model.get('age')}</div>
            </div>
        `
    }
}
