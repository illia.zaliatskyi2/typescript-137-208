import { Model } from '../models/Model'

export abstract class View<T extends Model<K>, K> {
    regions: { [key: string]: Element } = {}

    constructor(
        public parent: Element,
        public model: T,
    ) {
        this.bindModel()
    }

    abstract template(): string

    regionsMap(): {[key: string]: string } {
        return {}
    }

    eventsMap(): { [key: string]: () => void } {
        return {}
    }

    bindModel(): void {
        this.model.on('change', () => {
            this.render()
        })
    }

    bindRegions(fragment: DocumentFragment): void {
        const regionsMap = this.regionsMap()

        for (let regionsKey in regionsMap) {
            const selector = regionsMap[regionsKey]
            const element = fragment.querySelector(selector)

            if (element) {
                this.regions[regionsKey] = element
            }
        }
    }

    bindEvents(fragment: DocumentFragment): void {
        const eventsMap = this.eventsMap()

        for (let eventKey in eventsMap) {
            const [eventName, selector] = eventKey.split(':')

            fragment.querySelectorAll(selector).forEach(element => {
                element.addEventListener(eventName, eventsMap[eventKey])
            })
        }
    }

    onRender(): void {}

    render(): void {
        const templateElem = document.createElement('template')

        templateElem.innerHTML = this.template()
        this.bindEvents(templateElem.content)
        this.bindRegions(templateElem.content)
        this.onRender()
        this.parent.innerHTML = ''
        this.parent.append(templateElem.content)
    }
}
