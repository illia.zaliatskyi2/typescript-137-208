import { View } from './View'
import { User } from '../models/User'
import { UserProps } from '../types/IUserProps'

export class UserForm extends View<User, UserProps>{
    eventsMap(): { [key: string]: () => void } {
        return {
            'click:.set-age': this.onSetAgeClick,
            'click:.change-name': this.onChangeNameClick,
            'click:.save-model': this.onSaveClick,
        }
    }

    onSetAgeClick = (): void => {
        this.model.setRandomAge()
    }

    onChangeNameClick = (): void => {
        const {value: name} = this.parent.querySelector('.user-name')

        this.model.set({name})
    }

    onSaveClick = (): void => {
        this.model.save()
    }

    template(): string {
        return `
            <div>
                <h1>User form</h1>
                <input type="text" class="user-name" placeholder="${this.model.get('name')}" />
                <button class="change-name">Change name</button>
                <button class="set-age">Set random age</button>
                <button class="save-modle">Save User</button>
           </div>`
    }
}
